package com.twuc.webApp.domain;

import org.springframework.context.annotation.Scope;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Size(min=1)
    @Column(nullable = false,length = 64)
    private String name;
    @Min(1)
    @Max(10000)
    @Column(nullable = false)
    private Integer price;
    @Size(min=1)
    @Column(nullable = false,length = 32)
    private String unit;

    public Product() {
    }

    public Product(@Size(min = 1) String name, @Min(1) @Max(10000) Integer price, @Size(min = 1) String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}