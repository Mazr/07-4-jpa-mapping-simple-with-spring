package com.twuc.webApp.web;

import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.AccessControlException;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;
    @PostMapping("")
    ResponseEntity createProduct(@Valid @RequestBody Product product){

        Product savedProduct = productRepository.save(product);
        return ResponseEntity.status(201).header("location",
                "http://localhost/api/products/"+savedProduct.getId()).build();
    }
    @GetMapping("/{id}")
    ResponseEntity getProduct(@PathVariable Long id){
        Product product = productRepository.findById(id).orElseThrow(RuntimeException::new);
        return ResponseEntity.status(200).body(product);
    }
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity acceExceptoionHandler(RuntimeException e){
        return ResponseEntity.status(404).build();
    }
}